import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader';

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
var ambient = new THREE.AmbientLight( 0x74756f );
scene.add( ambient );
var directionalLight = new THREE.DirectionalLight( 0xffeedd );
directionalLight.position.set( 0, 0, 1 );
scene.add( directionalLight );

const renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

camera.position.z = 5;

const animate = function () {
    requestAnimationFrame( animate );

    renderer.render( scene, camera );
};

const controls = new OrbitControls( camera, renderer.domElement );
//controls.update() must be called after any manual changes to the camera's transform
camera.position.set( 0, 5, 1 );
controls.update();

const marronTexture = new THREE.TextureLoader().load( 'textures/Leather028_2K_Color.jpg' );
marronTexture.wrapS = THREE.RepeatWrapping;
marronTexture.wrapT = THREE.RepeatWrapping;

const purpleTexture = new THREE.TextureLoader().load('textures/Leather028_2K_Roughness.jpg');
purpleTexture.wrapS = THREE.RepeatWrapping;
purpleTexture.wrapT = THREE.RepeatWrapping;

const loader = new OBJLoader();

loader.load(
    'models/test_boots_midlle_poly_maison_felger.obj',
    function (object) {
        scene.add(object);
        object.traverse(function (child) {
            console.log(child);
            if(child.type === 'Mesh') {
                if(child.name === 'enveloppe_extérieure_Mesh.001') {
                    child.material.map = marronTexture;
                } 
                if(child.name === 'semelle_externe_Mesh.007') {
                    child.material.map = purpleTexture;
                }
                
            }
        });
    }, 
    function (xhr) {
        console.log((xhr.loader / xhr.total * 100) + '% loaded');
    },
    function(error) {
        console.log('An error happened:' + error);
    }
);

animate();